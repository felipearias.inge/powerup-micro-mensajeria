package com.pragma.powerup.usermicroservice.domain.model;

public class Restaurant {

        private String nombre;
        private String nit;
        private String direccion;
        private String telefonoRestaurante;
        private String urlLogo; //Nombres en ingles

        // Agrega los getters y setters


        public Restaurant(String nombre, String nit, String direccion, String telefonoRestaurante, String urlLogo) {
            this.nombre = nombre;
            this.nit = nit;
            this.direccion = direccion;
            this.telefonoRestaurante = telefonoRestaurante;
            this.urlLogo = urlLogo;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNit() {
            return nit;
        }

        public void setNit(String nit) {
            this.nit = nit;
        }

        public String getDireccion() {
            return direccion;
        }

        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }

        public String getTelefonoRestaurante() {
            return telefonoRestaurante;
        }

        public void setTelefonoRestaurante(String telefonoRestaurante) {
            this.telefonoRestaurante = telefonoRestaurante;
        }

        public String getUrlLogo() {
            return urlLogo;
        }

        public void setUrlLogo(String urlLogo) {
            this.urlLogo = urlLogo;
        }

    }

