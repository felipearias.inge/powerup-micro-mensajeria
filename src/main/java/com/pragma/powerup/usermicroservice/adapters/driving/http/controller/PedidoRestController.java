package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/pedido")
@RequiredArgsConstructor
public class PedidoRestController {

    @Autowired
    private IPedidoHandler pedidoHandler;



    @PostMapping("/llamar-notificacion")
    public ResponseEntity<String> llamarNotificacion(String idClient, String mail, String password) {

        pedidoHandler.callPhoneNotification(idClient,mail,password);

        // Enviar una respuesta si es necesario
        return ResponseEntity.ok("Notificación enviada exitosamente");
    }

}
