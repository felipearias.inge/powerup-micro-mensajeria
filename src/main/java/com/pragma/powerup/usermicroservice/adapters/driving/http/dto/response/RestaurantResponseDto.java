package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

public class RestaurantResponseDto {

    private String name;
    private String urlLogo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlLogo() {
        return urlLogo;
    }

    public void setUrlLogo(String urlLogo) {
        this.urlLogo = urlLogo;
    }
}
