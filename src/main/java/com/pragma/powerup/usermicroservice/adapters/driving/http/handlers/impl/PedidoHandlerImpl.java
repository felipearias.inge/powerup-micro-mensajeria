package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
@RequiredArgsConstructor
public class PedidoHandlerImpl implements IPedidoHandler {

    public static final String ACCOUNT_SID = "ACff989d5a073c11a7178f0eb1f14f688d";
    public static final String AUTH_TOKEN = "7547498f7d471f1ad0211fd69f6aa53f";



        public void callPhoneNotification(String idPedido, String mail, String password) {
        HttpClient client = HttpClient.newHttpClient();

        String endpointUrl = "http://localhost:8090/pedido/listo?idPedido=" + idPedido + "&mail=" + mail + "&password=" + password;//plazoleta

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(endpointUrl))
                .header("Content-Type", "application/json")
                .GET()
                .build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            String telefono = response.body();//Todo este codigo es para llamar el microservicion de plazoleta
            // Hacer algo con la respuesta
            System.out.println(telefono);
            String[] parts = telefono.split("/");//separa los elementos en arreglos depende de loq que tenga a dentro /
            String cellphone = parts[0]; // 123
            String pin = parts[1];
            //String ping = pedidoRepository.findPingById(Long.valueOf(idClient));

            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
            Message message = Message.creator(
                            new com.twilio.type.PhoneNumber(cellphone),
                            new com.twilio.type.PhoneNumber("+13614365529"),
                            "Su pedido esta listo, el pin para recoger su orden es: " + pin)
                    .create();


        } catch (Exception e) {
            e.printStackTrace();
            // Manejar excepciones
        }
    }

}
