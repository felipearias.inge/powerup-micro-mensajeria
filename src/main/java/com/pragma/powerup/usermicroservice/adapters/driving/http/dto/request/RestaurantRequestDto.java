package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RestaurantRequestDto {
    @NotBlank
    private String mail;
    @NotBlank
    private String password;
    private String name;
    private String nit;
    private String address;
    private String phone;
    private String urlLogo;
    private int idPropietario;

}
