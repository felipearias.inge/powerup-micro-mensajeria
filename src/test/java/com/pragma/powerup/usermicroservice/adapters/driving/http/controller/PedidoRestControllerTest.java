package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
class PedidoRestControllerTest {

    @Mock
    private IPedidoHandler pedidoHandler;

    @InjectMocks
    private PedidoRestController controlador;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void callPhoneNotification() {
        String idPedido = "idPedido";
        String mail = "mail";
        String password = "password";
        ResponseEntity<String> respuestaEsperada = ResponseEntity.ok("Notificación enviada exitosamente");

        ResponseEntity<String> respuestaActual = controlador.llamarNotificacion(idPedido, mail, password);

        Assert.assertEquals(respuestaEsperada, respuestaActual);
        Mockito.verify(pedidoHandler).callPhoneNotification(idPedido, mail, password);
    }
}